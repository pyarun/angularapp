import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthenticatedAppBaseComponent } from './authenticated-app-base/authenticated-app-base.component';
import { AccountModule } from 'app/accounts/account.module';

@NgModule({
  imports: [
    CommonModule, RouterModule, AccountModule
  ],
  declarations: [AuthenticatedAppBaseComponent],
})
export class MainModule { }
