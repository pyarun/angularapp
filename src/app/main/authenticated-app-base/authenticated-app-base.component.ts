import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authenticated-app-base',
  templateUrl: './authenticated-app-base.component.html',
  styleUrls: ['./authenticated-app-base.component.css']
})
export class AuthenticatedAppBaseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

