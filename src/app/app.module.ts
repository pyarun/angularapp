;
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ErrorPagesModule } from './error-pages/error-pages.module';
import { AccountModule } from './accounts/account.module';
import { MainModule } from './main/main.module';

import { AppRouting } from './app-routing.module';
import { AppComponent }  from './app.component';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

const myFirebaseConfig = {
  apiKey: "AIzaSyDzpBD0YYQnSBM2auNDWOFUX5fTDM2R5Gw",
  authDomain: "mf-tracker-59fc1.firebaseapp.com",
  databaseURL: "https://mf-tracker-59fc1.firebaseio.com",
  storageBucket: "mf-tracker-59fc1.appspot.com",
  messagingSenderId: "373824328356"
};

const myFirebaseAuthConfig = {
  provider: AuthProviders.Google,
  method: AuthMethods.Redirect
};

@NgModule({
	imports:[
		BrowserModule,
		NgbModule.forRoot(),
		AngularFireModule.initializeApp(myFirebaseConfig, myFirebaseAuthConfig),
  		AppRouting , MainModule, AccountModule,
  		ErrorPagesModule
	],
  
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
