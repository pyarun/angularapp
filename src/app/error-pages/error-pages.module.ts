import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Four04Component } from './four04/four04.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [Four04Component]
})
export class ErrorPagesModule { }
