import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticatedAppBaseComponent } from './main/authenticated-app-base/authenticated-app-base.component';
import { Four04Component } from './error-pages/four04/four04.component';

export const appRoutes: Routes =  [

	// {	path: '',   redirectTo: '/register', pathMatch: 'full'},

	{	path:'', component: AuthenticatedAppBaseComponent,
		children: [
			{	path: '', loadChildren: 'app/dashboard/dashboard.module#DashboardModule'},
			{	path: 'users', loadChildren: 'app/users/users.module#UsersModule'},
			{	path: 'mf-tracker', loadChildren: 'app/mf-tracker/mf-tracker.module#MfTrackerModule'}
		]
	},

	{	path: '**', component: Four04Component}

]


export const AppRouting = RouterModule.forRoot(appRoutes);