import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRouting } from './route';
import { AccountModule } from 'app/accounts/account.module';

@NgModule({
  imports: [
    CommonModule, 
    DashboardRouting, AccountModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
