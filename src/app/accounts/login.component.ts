import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { AuthService } from './auth.service';
import { Settings } from './account.service';

@Component({
	selector: 'login-form',
	templateUrl: './tmpls/login.html',
	providers: [
		AuthService
	]
})
export class LoginComponent {
	constructor(private router: Router, private authService: AuthService){};

	onSubmit(formData:any){
		var self=this;
		this.authService.login(formData.username, formData.password).then(function(result:any){
			if(result){
				self.router.navigate(['']);	
			}else{
				console.log("unable to login. try later");
			}
		});
	}
};



@Component({
	selector: 'forget-password-form',
	templateUrl: './tmpls/forgot-password.html', 
	providers: [
		AuthService
	]
})
export class ForgotPasswordComponent{
	form: FormGroup;
	email_regex = /^([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}$/g;
	passwordResetSuccess=false; // false will show the password reset form and true will display the success message.

	constructor(private authService: AuthService, private fb:FormBuilder){
		this.form=fb.group({
			"email": [null, Validators.compose([Validators.pattern(this.email_regex), Validators.required])]
		});
	};

	onSubmit(formData:any){
		var self=this;
		this.authService.forgotPassword(formData.email).then(function(result:any){
			if(result){
				console.log("Password reset done");
				self.passwordResetSuccess=true;
			}else{
				console.log("unable to login. try later");
			}
		});
	}
}


@Component({
	selector: 'app-logout',
	template: `<button class="btn btn-primary"(click)="logout()">Logout</button>`,
	// providers: [AuthService],

})
export class LogoutComponent{

	constructor(private router: Router){}

	logout(){
		var self = this;
		console.log("maa ki");
		// this.authService.logout().then(function(result:any){
		// 	
		// 	self.router.navigate(['/register']);
		// })
	}

}