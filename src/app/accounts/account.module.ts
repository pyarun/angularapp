
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule}   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AccountRouting } from './routing.module';
import { LoginComponent, ForgotPasswordComponent, LogoutComponent } from './login.component';
import { RegisterComponent } from './register.component';

@NgModule({
	imports: [
		CommonModule, FormsModule, ReactiveFormsModule, RouterModule,
		AccountRouting
	],
	exports: [
		LogoutComponent
	],
	declarations: [LoginComponent, LogoutComponent, ForgotPasswordComponent, RegisterComponent]
})
export class AccountModule{};