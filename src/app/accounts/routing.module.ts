import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent, ForgotPasswordComponent } from './login.component';
import { RegisterComponent } from './register.component';

export const accountRoutes: Routes = [
	{	path:"login", component: LoginComponent},
	{	path: 'forgot-password', component: ForgotPasswordComponent}, 
	{	path: 'register', component: RegisterComponent},
]

export const AccountRouting = RouterModule.forChild(accountRoutes);
