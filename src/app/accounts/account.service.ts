import { environment } from 'environments/environment';

/*
All account related settings/config can be added here.
These should be configurable and can be overridden by values in enviornment.ts
Any Account relating settings can be added in enviornments.ts under key named "accounts"

i.e.

accounts: {
	key: value
}

*/
export const Settings={
}