import { Injectable } from '@angular/core';
import { Observable }       from 'rxjs/Observable';

export class User{
	isLoggedIn:boolean=false;
	first_name:string;
	last_name:string;
	email:string;
	id:string;

	constructor(isLoggedIn:boolean=false){
		this.isLoggedIn=isLoggedIn;
	}

}


@Injectable()
export class AuthService{
	user:User= new User();

	login(username:string, password:string): Promise<any>{
		//Todo: Write Login http call logic here
		var success=true;
		if(success){
			return this.getUserInfo().then((resp) => this.user=resp);
		}

	};

/*This function should make a call to auth server to get loggedin user information*/
	private	getUserInfo(): Promise<User>{ 
		var user = new User(true);
		return Promise.resolve(user);
	}

	forgotPassword(username:string): Promise<any>{
		return Promise.resolve(true);
	}


	logout():Promise<any>{
		return Promise.resolve(true);
	}

}



@Injectable()
export class RegisterService{
	register(formdata:any): Promise<any>{
		return Promise.resolve(true);
	}
}