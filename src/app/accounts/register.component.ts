import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';

import { RegisterService } from './auth.service';

function passwordMatcher(c: AbstractControl) {
  return c.get('password').value === c.get('confirm_password').value
    ? null : {'nomatch': true};
}


@Component({
	"selector": 'register-form',
	"templateUrl": "./tmpls/register.html",
	"providers": [
		RegisterService
	]
})
export class RegisterComponent{
	form: FormGroup;
	email_regex = /^([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}$/g;

	constructor(private router: Router, private fb:FormBuilder, private registerService: RegisterService){
		this.form = fb.group({
			"company_name": [null, Validators.compose([Validators.maxLength(30), Validators.required])],
			"first_name": [null, Validators.compose([Validators.maxLength(30), Validators.required])],
			"last_name": [null, Validators.compose([Validators.maxLength(30), Validators.required])],
			"email":[null, Validators.compose([Validators.pattern(this.email_regex), Validators.required]) ],
			"password": [null, Validators.required ],
			"confirm_password": [null, Validators.required ]
		},
		{validator: passwordMatcher});
	};

	onSubmit(){
		var self=this;
		this.registerService.register(this.form.value).then(function(result){
			if(result){
				self.router.navigate(['/login']);
			}else{
				console.log("unable to register now. try later.");
			}

		});

		
	};

} 