import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-my-funds',
  templateUrl: './list-my-funds.component.html',
  styleUrls: ['./list-my-funds.component.css']
})
export class ListMyFundsComponent implements OnInit {
	myfunds: Object[];

  constructor() { }

  ngOnInit() {
  	this.myfunds = [
  		"fund 1", "Fund 2", "Fund 3"
  	]
  }

}
