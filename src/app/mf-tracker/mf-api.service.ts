import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MfApiService {
	endpoint:string = "https://mutualfundsnav.p.mashape.com/";

  constructor(private http: Http) { }	

  public search(serachkey:string): Observable<Object>{
  	let headers = new Headers({
			"X-Mashape-Key": "wIgAUyjVeNmshx2nMNaPpvLPGzApp1XdUGXjsnukOvezqu6Ume",
			"Content-Type": "application/json",
			"Accept": "application/json"
  	});
  	let options = new RequestOptions({headers: headers});
  	let data={
  		search:serachkey
  	}
  	return this.http.post(this.endpoint, data, options)
  			.map(function(resp:Response){
  				let body = resp.json();
  				return body|| { };
  			})
  			.catch(function(err: Response | any){
  				console.log(err);
  				return Observable.throw("errror");
  			})
  };

  nav(){

  }

}
