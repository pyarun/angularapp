import { Component, OnInit } from '@angular/core';
import { MfApiService } from '../mf-api.service';

@Component({
  selector: 'app-new-fund-form',
  templateUrl: './new-fund-form.component.html',
  styleUrls: ['./new-fund-form.component.css']
})
export class NewFundFormComponent implements OnInit {
	funds:any;

  constructor(private mfService: MfApiService) {}

  ngOnInit() {
  }

  onSubmit(data){
  	console.log(data);
  	this.mfService.search(data.search).subscribe(funds => this.funds=funds,
  																					function(err){
  																						console.log(err)
  																					}
  	);
  }

}
