/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MfApiService } from './mf-api.service';

describe('MfApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MfApiService]
    });
  });

  it('should ...', inject([MfApiService], (service: MfApiService) => {
    expect(service).toBeTruthy();
  }));
});
