import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { MfTrackerHomeComponent } from './mf-tracker-home/mf-tracker-home.component';
import { MfTrackerRouting } from './mf-tracker.route';
import { ListMyFundsComponent } from './list-my-funds/list-my-funds.component';
import { NewFundFormComponent } from './new-fund-form/new-fund-form.component';
import { MfApiService } from './mf-api.service';

@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, HttpModule, MfTrackerRouting
  ],
  providers:[MfApiService], 
  declarations: [MfTrackerHomeComponent, ListMyFundsComponent, NewFundFormComponent]
})
export class MfTrackerModule { }
