import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { MfTrackerHomeComponent } from './mf-tracker-home/mf-tracker-home.component';
import { ListMyFundsComponent} from './list-my-funds/list-my-funds.component';
import { NewFundFormComponent } from './new-fund-form/new-fund-form.component';


const routes: Routes=[
	{	path: '', component: MfTrackerHomeComponent, 

		children: [
			{	path: '', component: ListMyFundsComponent},
			{	path: 'add-fund', component: NewFundFormComponent}
		]
	}
];

export const MfTrackerRouting = RouterModule.forChild(routes);