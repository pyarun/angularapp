import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersListComponent } from './users.component';

export const userRoutes: Routes = [
	{	path: '', component: UsersListComponent}
]

export const UserRouting = RouterModule.forChild(userRoutes);