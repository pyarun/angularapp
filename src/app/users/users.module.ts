import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRouting } from './route';
import { UsersListComponent } from './users.component';

@NgModule({
  imports: [
    CommonModule,
    UserRouting
  ],
  declarations: [ UsersListComponent ]
})
export class UsersModule { }
